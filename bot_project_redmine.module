<?php

/**
 * @file
 * Redmine integration for bot_project.module.
 */

/**
 * Implements hook_bot_project_info().
 */
function bot_project_redmine_bot_project_info() {
  return array(
    'redmine' => array(
      'title' => t('Redmine integration'),
      'title_short' => t('Redmine'),
      'settings' => 'bot_project_redmine_settings',
      'scraper' => 'bot_project_redmine_scraper',
    ),
  );
}

/**
 * Part of settings form for Redmine integration.
 */
function bot_project_redmine_settings() {
  $form = array();
  $form['bot_project_redmine_url_regexp'] = array(
    '#default_value' => variable_get('bot_project_redmine_url_regexp', NULL),
    '#description' => t('Lookup issues when matched in conversation (ex. %example).', array('%example' => 'http://[\w\d\-]*?\.?drupal\.org/node/\d+')),
    '#title' => t('URL regexp for issue lookups'),
    '#type' => 'textfield',
  );
  $form['bot_project_redmine_url'] = array(
    '#default_value' => variable_get('bot_project_redmine_url', NULL),
    '#description' => t('Define the base URL used with issue lookups (ex. %example).', array('%example' => 'http://drupal.org/')),
    '#title' => t('Base URL (for issue lookups)'),
    '#type' => 'textfield',
  );
  $form['bot_project_redmine_nid_min'] = array(
    '#default_value' => variable_get('bot_project_redmine_nid_min', 0),
    '#description' => t('Lookup issues ("#1234" or "1234" as the entire message) at the base URL larger than this node ID.'),
    '#title' => t('Minimum node ID for lookups'),
    '#type' => 'textfield',
  );
  $form['bot_project_redmine_nid_max'] = array(
    '#default_value' => variable_get('bot_project_redmine_nid_max', 99999),
    '#description' => t('Lookup issues ("#1234" or "1234" as the entire message) at the base URL smaller than this node ID.'),
    '#title' => t('Maximum node ID for lookups'),
    '#type' => 'textfield',
  );
  $form['bot_project_redmine_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Redmine API key'),
    '#default_value' => variable_get('bot_project_redmine_api_key', NULL),
    '#description' => t('The API key to use for connecting with redmine.'),
  );
  return $form;
}

/**
 * Grab URLs from irc data and create messages with information from Redmine.
 *
 * @param $data
 *  The regular $data object prepared by the IRC library.
 * @return
 *  Array with messages.
 */
function bot_project_redmine_scraper($data) {
  $messages = array();
  $urls_to_scrape = array();
  $redmine_url = variable_get('bot_project_redmine_url', NULL);
  $api_key = variable_get('bot_project_redmine_api_key', NULL);
  $url_extension = '.json';
  if (isset($api_key)) {
    // We have to append the API key directly to the URL because it will not work
    // in request body.
    $url_extension .= '?key=' . variable_get('bot_project_redmine_api_key', NULL);
  }

  // Looking for URLs in conversation?
  if ($regex = variable_get('bot_project_redmine_url_regexp', '')) {
    if (preg_match_all('!(' . $regex . ')!i', $data->message, $url_matches)) {
      foreach ($url_matches[1] as $url) {
        $urls_to_scrape[] = $url . $url_extension;
      }
    }
  }

  // Maybe it's a numerical lookup instead...
  if (variable_get('bot_project_redmine_url', NULL)) {
    if (preg_match('/[^| ]#?(\d+)/', $data->message, $url_matches)) {
      $num = $url_matches[1];
      if ($num > variable_get('bot_project_redmine_num_min', 0) && $num < variable_get('bot_project_redmine_num_max', 99999)) {
        $urls_to_scrape[] = variable_get('bot_project_redmine_url', NULL) . 'issues/' . $num . $url_extension;
      }
    }
  }

  // Retrieve each desired URL.
  foreach ($urls_to_scrape as $url) {
    if (variable_get('bot_project_too_lazy_to_recompile_for_ssl', 0)) {
      $url = str_replace('https', 'http', $url);
    }
    // @todo I'm too lazy to recompile PHP with SSL support, and too Drupal-oriented to switch to cURL. I'm awesome.
    $result = drupal_http_request($url);
    if ($result->code != 200) {
      continue;
    }
    $issue = drupal_json_decode($result->data);
    if (!$issue) {
      continue;
    }
    $issue = $issue['issue'];
    // Save "seen count".
    $seen_count = bot_project_url_save(str_replace($url_extension, '', $url), $issue['subject']);
    // Build message.
    $replacements = array(
      '!created_on' => $issue['created_on'],
      '!status' => $issue['status']['name'],
      '!tracker' => $issue['tracker']['name'],
      '!description' => isset($issue['description']) ? $issue['description'] : '',
      '!assigned_to' => isset($issue['assigned_to']) ? $issue['assigned_to']['name'] : t('Unknown'),
      '!updated_on' => isset($issue['updated_on']) ? $issue['updated_on'] : t('Unknown'),
      '!done_ratio' => isset($issue['done_ratio']) ? $issue['done_ratio'] : 0,
      '!spent_hours' => isset($issue['spent_hours']) ? $issue['spent_hours'] : 0,
      '!author' => isset($issue['author']) ? $issue['author']['name'] : t('Unknown'),
      '!project' => $issue['project']['name'],
      '!start_date' => issue($issue['start_date']) ? $issue['start_date'] : t('Unknown'),
      '!id' => $issue['id'],
      '!subject' => $issue['subject'],
      '!priority' => isset($issue['priority']) ? $issue['priority']['name'] : t('Unknown'),
      '!seen_count' => $seen_count,
    );
    $messages[] = t('Issue #!id: "!subject" (Project: "!project"), Status: !status. !seen_count', $replacements);
  }
  return $messages;
}
